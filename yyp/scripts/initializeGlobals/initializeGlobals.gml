///@function initializeGlobals()

	gml_pragma("global", "initializeGlobals()");
	
	#region Enums
	enum PilotType {
		NAME = 0,
		AVATAR = 1,
		SOUNDTRACK = 2,
		LEFT_POINT = 3,
		RIGHT_POINT = 4
	}
	#endregion
	
	global.guiWidth = 960;
	global.guiHeight = 540;
	global.isFullscreen = true;
	global.enableAudio = true;
	global.pilotType = 1;
	global.pilotTypes = [ 
		[ 
			"bugz", 
			asset_avatar_bugz, 
			asset_soundtrack_bugz_chce,
			[ -26, -135 ],
			[ 31, -131]
		],
		[ 
			"papiesz", 
			asset_avatar_papiesz, 
			asset_soundtrack_papiesz_inba,
			[ -14, -148 ],
			[ 27, -142 ]
		]
	];