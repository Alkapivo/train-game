{
    "id": "074e81e8-4fcf-4744-b6b3-f20bd0cad8f1",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "asset_font_consolas",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "10647394-f2ac-4b63-88a3-332f0f36ae40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5dda305f-06ed-46dd-8d57-cdf94477f49e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 165,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d6eaa510-5c7f-4395-8711-e5022eef58f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 158,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b58e2465-c38d-44a7-aa51-fadf9e0fb6fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0f34d6c6-8dc7-45a9-8fd8-ec9b9cd5c717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5a320d48-6b4f-4fed-8dd7-3ae0c002db0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 120,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "be53d8d6-6618-4059-9f92-93908ffc0e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 107,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "68b64c02-7c31-4519-baff-af558e4fa5a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 102,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "528aaa54-d5f0-48e5-b746-dc4d078af52e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 95,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a8977b5f-21a3-44af-87db-02479c993cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 88,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f36c661d-eaeb-4cec-bc33-5b29b74efad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 170,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "175a3af0-b5f7-4136-8690-40f3d2da29bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 77,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0c379958-18ce-451c-90b1-90396db69446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 61,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "41d9089c-b7c0-4b71-b2c8-272ea02b28c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2333aeaf-3177-4cf4-9d30-46152d7a7ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 49,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ddc9442b-4353-46aa-83eb-520775c3cb57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 42,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "49b5baf1-b2f7-46d5-8596-c4d3a88327b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 31,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "31a57124-e34e-4980-8ef2-3a023b2a54b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6e4eba4a-0411-4ea3-b59a-e3ca1fd9eacb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2b6eef4b-7cad-4d10-8297-29bdedac4e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "78a10d5b-b726-4fde-aede-60005c1442ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1e3d9123-2aeb-45e0-aed2-7f7f6443e0f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 66,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "954038da-58ba-41b1-bcc5-0f0b7bc917ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "67d5dbae-c1a3-4960-a43d-39432ac99608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b20a93ca-1215-4ed8-ac68-0f4e142b796e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0a88bd1e-cd08-48c1-8cb2-dbe6f3fa881b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "26099ea4-2c5c-491e-9929-6ea39503bba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fd233fc9-dba5-40d9-b04d-ef03b3cd6025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "32970197-5d17-4587-b66a-ab67e5a58fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 165,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "17100d98-9ee6-4422-b40e-f309f9416de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 154,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fe9a0ca3-7605-48b4-8aeb-6cc8d51f2ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 143,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2302db76-38ae-4697-8b81-ed1618423355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c5828499-6f65-4614-a8d8-8edf7d0b364f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fcb3777e-7e21-4a0f-a835-d1f0fd273e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1886c268-a921-4b3e-b0c9-298a297ca16a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 89,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0a0cb29a-e9e9-4e28-aa9d-84d0588ee86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b66d5313-71a9-411a-b8ae-d97d0537b418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c386221b-e130-4525-8c0b-7cd56596205e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1ca83b8e-b2f3-4f2d-a9d9-a56353d27678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d22bc6d5-2eb8-48f0-90c5-55be9a4489ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 28,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1d27c891-22a7-427c-9bf5-6932d9d94716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7b36253d-7c41-4d7c-85ee-0f932399abb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "666a0ddd-416c-42ef-aee7-8e33cccabc1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9ab28221-bf2e-4c31-820e-c77123a55738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 235,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a86206b8-fdcc-4eec-bb60-cb58c83ac801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 225,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e0fd4697-9201-4f46-a5d1-a9d55fb3c64a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1eada10d-68be-449a-8af9-f84c5db73506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 228,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c247398b-f8da-4bd1-8e7d-439e1bb2da92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 214,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2cf451ea-fb8b-4b69-bb30-111c83a6d102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 203,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5edc3c94-9416-41cf-9d96-a20cf50c9dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9eb5ee02-423f-476e-aeaa-e690c2f0ce9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4f3b5d03-65e2-486d-a63e-c532a22df6b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "891d5dcc-a543-49f1-ae24-67a9a1963a5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "13a937c8-9e45-4d96-ba33-3b4502458004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0e96f20d-66e5-497d-ab2f-e85b26277523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0785e1ef-0b83-47b8-bdef-e676f096258a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d5e494a1-9b1f-4969-8672-882d495fe938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b1960c30-fc51-4e80-8911-b8f9c3281c70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "79e2cdf8-c5c9-422d-b0b1-fda3b0764d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6cbfa439-8845-44e7-96d2-d14b1f1e689f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f3954028-5e5c-410b-9c93-f77cff44c118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c46a782b-574a-42e1-a0b7-805d0e7afbca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4d7c4ed4-720d-4f29-b5b6-e116cd4cf614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "daf80c13-0a7f-4356-bce4-ab726eb1142b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f0592aac-7389-42b2-9f26-7aa416156eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "35353049-83a0-471a-9d1b-6196187ec5f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7a34712c-8eec-4850-a3a1-01e7f824660e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9494da5f-0a34-4dcb-b270-cfa4a67952cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e7310d5b-4ec4-4e4a-9884-87d13fec4edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6707357a-2d5d-4944-a7f0-c08babaf4bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2e0cda5e-8200-4ed3-a9a3-5c3ad47278df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1743870f-e206-4088-9da0-50e7bf8d3e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "93fa0527-3593-4016-8667-22c34e412c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "edcb6db8-3968-40ff-a9e6-32fde0504618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 12,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "86d2cd0e-5093-4b1e-bbbb-bcfbf51e4efd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 187,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5aefed1c-838a-41b2-a1b7-95492af66a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 178,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b8786929-84d0-4bdc-a124-ee229230d799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d0e86149-339a-4a3f-bee6-5f2e7b6ee3e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "62442e37-089d-4495-a72c-b3e4fbf885d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9e2f02e5-7d5c-4cce-9016-fc67afafca4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 140,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0a4d544e-5eac-4eef-b516-5125756ef944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2e1867cc-a5d4-4cfe-ac29-17c0c43f61af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c62923c1-d5e7-4ed7-bda4-699af91c49d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 113,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "443472ad-cb09-49d9-b066-da714e23271a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 193,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e89e2eee-7d9c-471a-8913-f99a9fc5b064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 106,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "fe000508-e556-4445-b378-5880d8661bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 88,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "da5d0d70-66d8-41f6-b791-960a34890386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d8b3cb7e-2f82-4e0b-b003-17d6a87b12d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "be3a9710-2958-4733-9203-7f785b3b8b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d988de36-172a-454a-9982-88c6b882989d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "78ba08ad-b2f8-49e6-933e-8f35b78b0392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 34,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6b9615bd-85f2-4e17-85aa-4cbb57b2b142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 27,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "47823124-bec6-49b9-8b38-e7ee78f607b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 23,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0bc4c29c-a5c4-4290-9352-89adefa0be00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ccd2387e-b769-43e1-98bf-456ba8de7e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 197,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "20bf025c-5300-4727-8fbe-e8239c94f8c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 208,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "0216e763-76eb-4060-a966-025a54b0945e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "cd284749-7122-46ba-a630-eba360a6c57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "50fdf861-2a3c-487f-988e-65403cbf3d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "e274798e-2715-4055-b7e6-b0e21a9a566c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "40797bbb-1733-4170-b2b8-332252a2f570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "6507d630-ab41-4f8e-aa46-2256a0ad95be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "e4a90144-ebb0-4ca8-a1a3-a56d70e6ea21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "3dfcd715-58ee-4fd7-b0ee-b91bb5938114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "b022974a-2410-4bb7-94ba-381503e54235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "a30a58f1-b57e-44b9-9bc9-094e6f23629b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "dfc600bf-f09f-4fd4-a16a-f46eb62d7350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "2ece12a6-3271-4792-b6e5-5f9a16b97334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "6ad9f08e-ee03-4354-abb3-4ded71ae6e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "aa403da1-a8ee-495d-94d8-f70eafbb7213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "95cb441c-0caa-4b6a-bf81-c3d0766fe8bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "1d9eb012-40b0-48d1-9e2b-9dd2e13ccb38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "3c4e1f47-dff8-4a08-a7b6-12fabd62c023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "403f98c6-9a37-43c1-af32-67ae3a77dc81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "b3c190f0-6561-4fb6-8a1a-ea2ef9166831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "97f90246-4918-4f40-bf1d-0463c2cb46d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "c34a03d8-7d7e-4abc-ac32-bac8fb639d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "4372aa3b-2ce5-4244-ad1f-2f509ad02071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "7a36858f-73f6-42ea-ac6e-f47a1ab3b934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "23701c1c-d1b0-425f-a0dc-8414d72dceaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "7fcf8334-9659-47da-b443-0f1fdc579d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "2d734a6e-6d74-4bb1-82d6-d05095db6471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "8a9dd158-8fcf-426a-9852-d89a6f33d53b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "2d51f3f4-03bb-4c50-b94c-e393fb564b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "96f0f793-7d4f-4833-a1cf-fb225a155919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "a0e84494-9f6e-4e7d-ba63-73657ff92bad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "0f5cae3b-6421-46bf-965a-fe2cfd22eabd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "72c56b17-4b5a-4ccf-a3d3-f1ee797dd219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "982860f0-f874-4908-ae46-d907090f990d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "78da6eea-cc58-4e54-94e1-a54f216ce798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "1d3405bd-f866-496c-9177-fd43d6882e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "f965118f-27e1-499d-a775-1dd4765481c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "41bfecf5-cf8c-472f-8461-b4080a23f051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "869113ff-3fb0-419a-a689-4cd240d40771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "944a0ed6-554a-43ec-a0d9-30824fc458cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "3ccf2d6b-fcdb-4c44-ab93-ab0fa3060c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "4f7340cb-a622-4e9f-b7c1-5a62b641a42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "8ec5070e-a67b-4891-a16a-db5db1d6bb59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "882ab7ed-3463-4ed5-aff0-0c3705770e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "b7e1dd39-c7b7-42d1-a7eb-1e8c43b389fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "7024f6bb-2980-42ab-8da7-50034a906f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "141d443e-b7ef-41ae-9a0b-f35b1851c820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "885e5d85-3487-42e5-a593-e6267a1b8d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "789afeff-0f6b-4f9b-bb2d-888e14117403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "d6c3b192-bcdf-45f8-84f0-2222c5f1e40f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "88940353-b4eb-4343-86ef-0ada72b05635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "9a98f93c-ffb8-4807-8476-d2c32d8e4cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "d4643cb2-05d5-49fd-b2f7-d484bab817b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "ded075ba-7719-4d54-ab1e-64b4fd18973d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "4027f377-0cb6-4d52-971d-97162f725c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "6a3274e6-2872-4261-bc06-5fefe82ae7a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "905f2bdb-7477-4912-b204-138056ec284d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "0daa727b-b237-4cfd-89c6-a9378cc586f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "c7096f99-7521-4250-a52f-49c35fdb8d68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "0f428e88-5867-4827-adde-2259eb232eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "53d953cb-81e5-42d1-ad42-ba60dc2ee0c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "f07c1d96-1e2d-4596-9b5d-de7262a79d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "960151a7-0637-4574-9fe7-2fe85ee9fa2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "e0d82d5b-3708-4538-a6f3-7a8077eda4d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "db48eca2-7c9f-432f-b108-8be1f74c901c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "753382e9-cfe2-4f4b-bf40-7f99fe432608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "6aeadf4e-4463-478b-bd4c-c34222415afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "c773fbf5-f74c-4222-a8bd-68ad5b2eb5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "a6693026-4bab-4dac-9878-f521bd881a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "374581fc-cd75-4c5a-8253-c8bab2722f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "48805d7a-6387-48d2-837f-becb8fc5aec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "1e24bae5-79d2-4060-97ea-7cb436042261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}