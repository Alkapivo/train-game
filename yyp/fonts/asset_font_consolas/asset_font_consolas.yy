{
    "id": "074e81e8-4fcf-4744-b6b3-f20bd0cad8f1",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "asset_font_consolas",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5a3308e6-fead-4e66-8e35-c287b2a74abd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "195c0923-1d78-4069-b235-1f9f00613eb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 51,
                "y": 83
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "60aa75d1-c82c-43dd-b0a7-c01f2d80f862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 42,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d35b5388-cbd4-4310-916a-4e9c7124565b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 28,
                "y": 83
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3a760997-2185-4012-aa31-9baa8617837b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 83
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c44f138c-fab8-48ff-b2f7-b0c151ffef56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3f7c1195-66cb-4f54-b4ff-2e93222870c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 240,
                "y": 56
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f8bd7d1e-d4e6-4385-90fe-00674f98f2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 235,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9b702cbd-8c29-4ad8-b500-3e2ef59a6a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 227,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "85db0c40-72bb-4e09-9e37-985ef687cc1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 218,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f45f89e7-1f8b-4972-afe0-9046a27dbd8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 57,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "01339622-167f-4d69-8c78-1d3f0eaf8c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 205,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f8d88471-a846-438b-b32e-4293013eaeb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 186,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "40370d6a-0ec2-4913-948d-1f7ef9aba017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 177,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f7f380cc-5e17-4bff-ae75-ac2f5fd64cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 5,
                "x": 170,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b9d11277-0016-4578-9681-c952e44096f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 159,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "52f5a652-1281-427d-b014-0ed7e67bf54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 146,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "22b91217-0e09-49b2-8532-b140e05e2e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d04bf931-8334-4477-a5d1-a127a301bb50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ef8131c4-059d-412e-9489-14bf46a6d9f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a1a55a04-7129-4467-995a-384ba9203fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 97,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1cbd1881-fcff-4835-8fd8-b7c05606e776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 194,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6858cf25-9840-4fe7-ac88-b782bf8eb755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 68,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f62fa349-305f-4bca-94d9-e3ed7b5bd105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 80,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d1abcb83-7dad-4fbb-9fbd-8e622f7469e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 92,
                "y": 83
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1f4fb7de-aa33-48db-8b73-1eebad9e90af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 87,
                "y": 110
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "79e94aca-89f0-42f4-ab94-7716dd9ddb49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 81,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "58ccf50d-c5ce-4ab9-9429-69e0af00ab00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 73,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c24bc5bc-88c5-4c2b-8230-563bbb8be009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 62,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a3034e18-9ea8-471b-b547-4aae1df771ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 110
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f81fb4cb-ef12-41c6-ba2b-95a37ec46be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 39,
                "y": 110
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "02399d9b-e37c-4f94-9085-ec1c017c2d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 30,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ef3f3eb1-620f-4dad-9b10-47262b45abe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 110
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b72d6789-2348-44aa-9f17-32c8b9e28671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "33e7ba65-d0ed-4938-91d9-06182951c52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 232,
                "y": 83
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d97d75c3-27ee-46c5-a020-0f77ebb0c92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 219,
                "y": 83
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "83b06438-409f-4412-9c50-26926cd3e6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 207,
                "y": 83
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "906ca222-4852-4e96-9df8-2c6bc8d99886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 197,
                "y": 83
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1145e9a1-76ef-409c-9ac1-daecff00f4f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 187,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "70cc095e-2b0c-4027-bda0-425681d4d17c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 174,
                "y": 83
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4a72b83d-eaab-4a1e-a016-a9508e536273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 162,
                "y": 83
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "584337ae-b8e3-4d0f-b691-221c4b88421f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 151,
                "y": 83
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "780b25f4-169d-40b6-8d5e-6381494c8cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 141,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "375ab54e-6c0d-4a3a-a4e5-fe9ee07c582f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 129,
                "y": 83
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d583c48e-cbc3-466b-a6db-fc4b03c1a401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 118,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fc3497ca-6076-4450-a304-77ddb2063eaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 104,
                "y": 83
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "51e39cde-a864-4eda-a51e-1bec5628ff3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 85,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "db0c1b32-decf-43f4-ab3d-d813da551cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 72,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5af6dfa1-2b92-4d69-bde1-63f884e3c42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 60,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5ee03f07-b36c-4432-86b2-5b9ace10bcdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 22,
                "y": 29
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7d82b3a9-51d6-401a-b1c3-25910e8698c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5577ac19-b3dd-42b2-8290-0e2b4e6e7dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cf5bd794-942e-439b-8036-d33442841d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "71b95efa-d004-457a-90f1-cf01076d4bdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3a67813b-4eb8-46f0-a02e-88588525769d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d1464e1c-53d9-4793-877c-4501a3e6c9ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f96892f0-1a2c-49b8-8908-82d696c41e61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "59eb7084-77b6-4487-af71-507975388bc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4bee9146-9467-4c8f-8cfc-b0dda03466bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "84cf723b-e91b-420a-9546-4de292867e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 14,
                "y": 29
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7543f8d8-6dbd-4c55-8a12-22f8a7089a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a842f703-3369-482b-add4-f82d34ded0c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "654d2b04-89ac-4f96-af3d-0e49d495c5c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bc471e5e-8c4f-4373-acb1-a8076354e560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2eb7f54e-5d33-48d2-b8da-e764f00a035f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "bf7b562b-a5b2-4974-938f-07bc1e2991dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "11e88e42-5809-40be-9773-1963484de051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "06e77693-17ac-4776-a0e7-8a5482f2c16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "12413f23-97b4-48f6-ae02-08218ed159b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "112247b4-2e63-469b-bc9a-bc86076a1130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d84121d3-2afc-467c-83c9-776dba3028ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1b4af64f-8e3d-488b-8d91-b08257aa6fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 36,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ce7b1142-3b4f-410e-8e2d-0b2c1ad9e9d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 162,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "32b96d4e-96fa-497d-9009-de645b359019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 49,
                "y": 29
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2f4fc647-2ffd-40e0-b658-ee7d9dd5827f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 39,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "13322f60-85fe-4447-9aa5-2d6e993457e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 27,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "49b06dd5-cb14-4ab3-9b6e-3ae1666dc4ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 15,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "477c5a06-e526-4003-9a11-7dd43b89ddfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7e4a8e47-2ff1-40d3-bb8b-f34766f000cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 232,
                "y": 29
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "14030fee-c6d1-4f72-a04d-ffadf845e508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 219,
                "y": 29
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5fee55aa-61fb-4b54-ae36-4aafc2b32778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 207,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5c1eca40-9f71-4705-88f1-f19ff21df555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 196,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d011476b-421d-4214-a5a0-c963d95446a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 185,
                "y": 29
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "56e53baf-d1c8-48b5-baaa-6e0e42f0addb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 49,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "63d7a6e9-ae4b-4659-a081-ec3c86e82034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 173,
                "y": 29
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4fbac2a9-ed11-422e-b755-afa14ef251ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 151,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "eb67ac5d-6be1-4e2c-9a94-ce1efd3ceb10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 138,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "36f6890a-05e6-4508-b941-1b4ab3e62bfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 124,
                "y": 29
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c6288ef7-ceba-4444-84a4-58ddc67b5f7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 111,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "47670c3e-5862-4916-b5f0-46640e00c23d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 98,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "328d9c5b-6eb4-4bc8-9191-e8c7c675c89f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 87,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "813f769d-3d07-4dad-a2df-6e499e4312fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 76,
                "y": 29
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2b2be408-884f-4c12-90cd-eee3c5eead31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 4,
                "shift": 12,
                "w": 3,
                "x": 71,
                "y": 29
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "946fb335-1845-4a2f-88af-470cc291c90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 61,
                "y": 29
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8e2c15e7-fbbf-494a-b20a-7e355f8176ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 99,
                "y": 110
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c28ec1d5-4a1a-4b42-b9de-57458e46b6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 25,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 112,
                "y": 110
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}