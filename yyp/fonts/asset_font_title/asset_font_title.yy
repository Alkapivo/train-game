{
    "id": "e1273490-0a8e-40ac-a5f4-765a1feef5c2",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "asset_font_title",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "80c3b937-6e8d-4249-8092-3d1d85bf18a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 48,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "52cf083c-1a8f-4758-8b27-d09dd2d89b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 48,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 274,
                "y": 102
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a3454f70-bc7e-4383-8b82-f0bcbf8a9735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 48,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 261,
                "y": 102
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dcd88c6b-a875-4a55-b26e-00f616484708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 48,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 229,
                "y": 102
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d50f8c22-be8c-426b-866d-23b262e86250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 20,
                "x": 207,
                "y": 102
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "05bce457-0e82-4e5e-8d05-189e6228fc7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 48,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 179,
                "y": 102
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "86e0983f-8d99-415b-898a-3093ab0a20c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 48,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 156,
                "y": 102
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ac630656-4a46-43fc-9436-083096f3f3b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 48,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 149,
                "y": 102
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0189251a-c47e-47da-b296-f6d4e7cc0cfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 48,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 136,
                "y": 102
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "23a91f26-b0ac-4022-a3b8-c8e9ce28d37d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 48,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 123,
                "y": 102
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e9850fd5-149e-4085-8ef1-930158cbda0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 48,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 280,
                "y": 102
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b6b5db00-dbea-45ef-9b49-4929e6ba28eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 48,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 105,
                "y": 102
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c779360c-528e-4f86-bc75-f17b584e447d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 48,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 77,
                "y": 102
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "88af89a9-71f0-4f99-8764-3be0c866e6e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 48,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 63,
                "y": 102
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b76bd4fd-968d-496c-bc29-a512819c4329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 48,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 56,
                "y": 102
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f79fafa6-486d-4ef8-9a67-b0d03b60917e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 48,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 102
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "65be6772-a04b-4367-a804-302236750127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 16,
                "y": 102
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "61dfe004-18ce-40ad-a4b9-868df34da886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 48,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "076171a6-c610-486e-b8a2-b6d16dc135c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 481,
                "y": 52
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "cd2e4086-7a40-4e18-9107-7a6d8f39caa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 462,
                "y": 52
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "19eca75b-16e3-4b8e-8248-c0f5d6cbfb35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 48,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 439,
                "y": 52
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a28c08c1-645e-47fd-a521-038ad4871ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 85,
                "y": 102
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "aac37363-0e50-4647-a59f-f5361213eb47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 299,
                "y": 102
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "358d3970-b4a1-414d-b889-d5ae741c1b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 319,
                "y": 102
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7abe756f-8477-4dea-852d-a27325d576d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 341,
                "y": 102
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e63e8c4f-b8f5-4ed2-b506-072ba41c4b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 269,
                "y": 152
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "25069703-e82e-4bf4-86c2-2b30bf29ecbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 48,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 262,
                "y": 152
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "81fbc055-f568-453e-8cd6-d287b04b4fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 48,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 253,
                "y": 152
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6e9470bb-821c-4354-938b-9e960d515e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 48,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 240,
                "y": 152
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5012a20c-3b88-49a1-a8c1-4eb697e974eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 48,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 223,
                "y": 152
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e080f81c-81df-4b09-bec9-cd9bfaa5bc5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 48,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 208,
                "y": 152
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fb5e6689-983c-44fd-813d-0a1631c152d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 48,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 189,
                "y": 152
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "be693776-b0a9-4ada-8f4a-b08769f6129c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 48,
                "offset": 1,
                "shift": 33,
                "w": 29,
                "x": 158,
                "y": 152
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c2a7430a-7923-4f16-9926-59e981d1d4ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 48,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 134,
                "y": 152
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c0a2ba9c-1ca7-4a41-ab77-abbc8595698c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 48,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 114,
                "y": 152
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "443b0463-8750-4336-a388-d20b28872b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 92,
                "y": 152
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e94cd435-1570-482d-bbe0-32d847224d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 48,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 69,
                "y": 152
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c0b048bf-750c-448d-a0d9-7cf8f4e3a2a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 48,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 48,
                "y": 152
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dd79c347-15e7-4c04-b691-546b86bc7750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 27,
                "y": 152
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0d946ba7-745b-40f3-a6b2-871c98692377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f41b954e-2d57-4cf2-9bb3-f3c86c7573a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 48,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 477,
                "y": 102
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8fa35519-7670-4cb9-960e-cbe19b7f1756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 48,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 457,
                "y": 102
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "dd1a5563-e129-4485-9d0c-356e230ae760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 48,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 433,
                "y": 102
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "89817dd9-fb3f-41f9-bd61-ec187ea8c901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 48,
                "offset": 3,
                "shift": 21,
                "w": 19,
                "x": 412,
                "y": 102
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8c317f63-dcd6-46d8-9449-aaacaeff5b70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 48,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 392,
                "y": 102
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "be627cf4-2954-494c-8c79-1587cccd29d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 48,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 361,
                "y": 102
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fa5ffa3b-7ff1-4cb3-af8d-06dcc211b3d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 48,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 412,
                "y": 52
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "146ba7c0-436c-41eb-be6f-2af4f375e390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 48,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 384,
                "y": 52
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f81bcefb-826e-4bba-9fad-c7bf4e1af8c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 48,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 365,
                "y": 52
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7c8f8130-fa62-46de-8e47-3b3c13b521f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 48,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 449,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2cef3b7e-de3e-46e4-9b49-fbab6cfa3b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 48,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "21a65778-12d4-42af-9718-659f62cac45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bb1650dc-a939-4184-ab1d-ce11fbeb3d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 24,
                "x": 368,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8df9444c-2c5d-4458-98a3-a8794854bcd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 48,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "64e13407-a386-4523-a24a-ba9b32d8021e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 48,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 321,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "89a6d0a5-4af5-4f09-90b0-af7a31c7c32a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 48,
                "offset": 2,
                "shift": 36,
                "w": 34,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "572d073f-cc57-4970-9e7d-84005bed634e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 48,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 259,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3d93c007-bcc4-49f1-bb7d-95e0d703d35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 48,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3c0a7ec1-504c-4811-bdbf-b77dab4f1817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ccd9ace3-fd33-4917-86d4-dc249723fe81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 48,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 438,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1a589010-91a4-4cfe-8072-099269018677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 48,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "20f4d610-963f-450a-b95b-ea18f8091535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 48,
                "offset": 3,
                "shift": 13,
                "w": 9,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "67904cbf-08fc-49b8-9c39-77e7e5e29e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 48,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f1c6c50b-45e5-4900-ab56-aadede67759f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 48,
                "offset": -1,
                "shift": 22,
                "w": 24,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6b7edde1-9d94-42ca-a7f6-2d54f1c6a569",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 48,
                "offset": 2,
                "shift": 19,
                "w": 8,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6f337768-7aa0-43e4-abe6-2cb0a5809384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 48,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bcd3ead0-b5b4-467d-ac78-c0d3894e1340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 48,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "52391168-957a-4964-adb7-8fdb2bfe8c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 48,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fa5f0291-85f4-4001-aedd-bb9d377cdcd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9b61604d-c947-4229-9180-628042e35d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 48,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0f91149d-958b-4479-ad88-25364bbfcd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 48,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cc137422-ceba-48b9-8265-6dadb1f92a21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 48,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3bbac40f-1bfc-4924-a95d-28f12c4d598f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 48,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 156,
                "y": 52
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "361e0d46-eb06-44a3-a507-522add61dcbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 48,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 500,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e1593267-d90d-4195-b7d6-b6c90c2d2b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 48,
                "offset": -1,
                "shift": 14,
                "w": 13,
                "x": 332,
                "y": 52
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "96c5958b-730c-4a56-94d8-54c816e3d9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 48,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 313,
                "y": 52
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "07afb8c1-cba7-4a82-8201-979808de8209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 48,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 306,
                "y": 52
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0a093070-fd6d-4853-bde0-951d37676ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 48,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 280,
                "y": 52
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "70ed8c3e-ddd3-4c2a-a8f0-01da66ca03f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 48,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 262,
                "y": 52
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "649cb57b-1fbf-4c02-9830-19d820da3a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 48,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 244,
                "y": 52
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9ec7e0d7-ebc4-4503-a0a7-a8d332eb9a4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 48,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 226,
                "y": 52
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3a3a9bf9-6ead-4bc7-be94-c4e2cedbfa34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 48,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 208,
                "y": 52
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "90b9827f-de3a-4c29-babc-2c1c51a8b6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 48,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 192,
                "y": 52
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8f1dd968-9f2f-40f3-a912-9d206214ef8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 48,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 347,
                "y": 52
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6f09a36b-44d5-4fa8-a468-3cfb33c655ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 48,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 175,
                "y": 52
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f6f2049a-3b82-408c-ac94-5bd9c1e5bc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 48,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 138,
                "y": 52
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8bf5a9b0-8fdd-44f1-8780-fe516009ac3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 48,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 120,
                "y": 52
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ba05fc75-9dc1-4da8-a5b6-791d1188d3f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 96,
                "y": 52
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8d02ea29-f5ee-46d4-bd43-615689130a2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 75,
                "y": 52
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d7712e70-ce94-4f9e-892d-c800d3f86b41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 48,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 54,
                "y": 52
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0be598a4-4d7d-46da-96c6-72ebc0bbc694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 48,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 36,
                "y": 52
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a9642610-a148-4ac1-984a-21d914f0fea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 48,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 22,
                "y": 52
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "450e8851-5fb1-4fdb-a932-b05575fb5a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 48,
                "offset": 6,
                "shift": 15,
                "w": 4,
                "x": 16,
                "y": 52
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b3055055-3243-4066-bd69-36c837d64b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 48,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "abd1cd8d-d592-49b1-9fe1-5b770b6a889c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 290,
                "y": 152
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "0a122b5b-464b-4449-bfc5-23e5caf76e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 48,
                "offset": 6,
                "shift": 21,
                "w": 9,
                "x": 311,
                "y": 152
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 26,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}