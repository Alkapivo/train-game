{
    "id": "a1af5468-dfc5-4d00-94d9-f3f63e024441",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 29,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13e703ca-73a2-44ec-85f0-11eb01fc5204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1af5468-dfc5-4d00-94d9-f3f63e024441",
            "compositeImage": {
                "id": "d905b193-9249-41cf-936f-0beaf8cfac73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13e703ca-73a2-44ec-85f0-11eb01fc5204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10a0e63f-3898-433c-8d5f-3c9abdab00b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13e703ca-73a2-44ec-85f0-11eb01fc5204",
                    "LayerId": "5239b787-92a1-4548-ac1f-b3550bf09593"
                },
                {
                    "id": "69835c11-8654-451e-89be-ae3cd6e2c581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13e703ca-73a2-44ec-85f0-11eb01fc5204",
                    "LayerId": "efaf5c27-1dd1-47ff-b624-b0d582304b54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "efaf5c27-1dd1-47ff-b624-b0d582304b54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1af5468-dfc5-4d00-94d9-f3f63e024441",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5239b787-92a1-4548-ac1f-b3550bf09593",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1af5468-dfc5-4d00-94d9-f3f63e024441",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 14
}