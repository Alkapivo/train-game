{
    "id": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_shroom_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 37,
    "bbox_right": 82,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6ccdd07-9c34-41e5-883d-b0bb01482e07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "compositeImage": {
                "id": "7aed8a19-10bb-4cfa-9775-55cd00250a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6ccdd07-9c34-41e5-883d-b0bb01482e07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08a7207b-d94c-4115-a9dc-3982c320dc6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6ccdd07-9c34-41e5-883d-b0bb01482e07",
                    "LayerId": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6"
                }
            ]
        },
        {
            "id": "032f5a03-7552-4488-9bf2-edb46cfb93d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "compositeImage": {
                "id": "fbb0258a-4c1a-4d20-b783-b7c6e699660d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "032f5a03-7552-4488-9bf2-edb46cfb93d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc379f74-f6e8-4517-b349-f0d0f46a8eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "032f5a03-7552-4488-9bf2-edb46cfb93d9",
                    "LayerId": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6"
                }
            ]
        },
        {
            "id": "37d95194-f0fa-42b4-a3f5-a4170148f5a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "compositeImage": {
                "id": "332351c4-c0f4-4d6b-aa12-37a60d01cc47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d95194-f0fa-42b4-a3f5-a4170148f5a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c30e37-2143-4bee-9b8f-c74922fce728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d95194-f0fa-42b4-a3f5-a4170148f5a5",
                    "LayerId": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6"
                }
            ]
        },
        {
            "id": "34aa45b6-5f32-4281-8b4c-0a846727b3b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "compositeImage": {
                "id": "2548cf2a-3d80-4163-891d-6ce2316f4f0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34aa45b6-5f32-4281-8b4c-0a846727b3b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8e0fbf-3c02-497f-8b09-0c3cdd6eafda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34aa45b6-5f32-4281-8b4c-0a846727b3b5",
                    "LayerId": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6"
                }
            ]
        },
        {
            "id": "7b24de05-b237-4719-afb7-188600db9a6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "compositeImage": {
                "id": "e629d43d-fe86-41f2-b2f8-4bcf98459786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b24de05-b237-4719-afb7-188600db9a6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6279aff3-7636-46ce-ae4f-2cb12386926a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b24de05-b237-4719-afb7-188600db9a6f",
                    "LayerId": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6"
                }
            ]
        },
        {
            "id": "52a663ce-c187-410b-bff2-5180ff91c467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "compositeImage": {
                "id": "a7fb9c77-7c63-4c38-a69b-d07ff88f35df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52a663ce-c187-410b-bff2-5180ff91c467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6625d925-3c08-48a7-a195-aeaaf9fe29ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52a663ce-c187-410b-bff2-5180ff91c467",
                    "LayerId": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6"
                }
            ]
        },
        {
            "id": "0e25430b-b327-4862-a827-5d263c345dd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "compositeImage": {
                "id": "b124939a-8c73-4e4f-8570-a84065b8b0fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e25430b-b327-4862-a827-5d263c345dd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "439945bd-3008-42da-be2c-602390b6c9fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e25430b-b327-4862-a827-5d263c345dd4",
                    "LayerId": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6"
                }
            ]
        },
        {
            "id": "342e87aa-95ce-492c-b9cc-27dbc814218c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "compositeImage": {
                "id": "0768ac22-5c7d-4c8f-b12b-bab9dc0a7a72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "342e87aa-95ce-492c-b9cc-27dbc814218c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02d97b77-a34a-4169-b5e2-486d861382c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "342e87aa-95ce-492c-b9cc-27dbc814218c",
                    "LayerId": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 233,
    "layers": [
        {
            "id": "5c2a3a9c-fbd9-4d60-bf02-77dbb373c0b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 116
}