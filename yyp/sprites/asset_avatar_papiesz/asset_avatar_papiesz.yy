{
    "id": "2e068101-c5bf-4be6-aa81-c290ac6d5d9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_avatar_papiesz",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 306,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7889ae9-d86f-44b8-b16a-da4be87c5fdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e068101-c5bf-4be6-aa81-c290ac6d5d9e",
            "compositeImage": {
                "id": "20d16612-0a00-49a7-a675-b0e0ccc36f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7889ae9-d86f-44b8-b16a-da4be87c5fdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9587ff1b-ba14-4b92-b503-f8bf16e8c75e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7889ae9-d86f-44b8-b16a-da4be87c5fdc",
                    "LayerId": "69e68c91-8f56-466f-99bc-a460f2a67ea6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 307,
    "layers": [
        {
            "id": "69e68c91-8f56-466f-99bc-a460f2a67ea6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e068101-c5bf-4be6-aa81-c290ac6d5d9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 250
}