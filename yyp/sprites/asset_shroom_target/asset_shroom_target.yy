{
    "id": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_shroom_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e76c84f3-5f54-4044-a533-eac939da37c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "97f9bba0-c39b-489c-9bd3-59c26bb5c7cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e76c84f3-5f54-4044-a533-eac939da37c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a11e3d-6b7b-46d5-88df-3e49f89c97fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e76c84f3-5f54-4044-a533-eac939da37c8",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        },
        {
            "id": "b578fc0b-8b78-4133-9bcd-1ec45c79ed91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "efe22e81-4c0f-4c98-a65c-2ba759bd5ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b578fc0b-8b78-4133-9bcd-1ec45c79ed91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3558a1d1-aacf-4245-b0fe-55c63d67d3a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b578fc0b-8b78-4133-9bcd-1ec45c79ed91",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        },
        {
            "id": "efb77e08-af4d-4e96-b606-7c4a047e8eb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "8a571783-2275-47a8-893c-cc34f266ea7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efb77e08-af4d-4e96-b606-7c4a047e8eb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56177315-3c1f-40fc-b720-be70f3ef4b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efb77e08-af4d-4e96-b606-7c4a047e8eb4",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        },
        {
            "id": "3bdf418d-2ffb-437b-aa98-aadad940a893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "add50f73-5817-4ed5-9cdf-93206d53e06b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bdf418d-2ffb-437b-aa98-aadad940a893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df16d397-0900-4911-a80b-7d3a626f1fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bdf418d-2ffb-437b-aa98-aadad940a893",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        },
        {
            "id": "9b91334a-9fae-44bb-ba73-02c7378b3d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "5852f9e6-d95d-4108-bdbd-dccfa172e700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b91334a-9fae-44bb-ba73-02c7378b3d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea9caf1-773b-42a9-8c58-3551e24c6cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b91334a-9fae-44bb-ba73-02c7378b3d41",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        },
        {
            "id": "d614d62c-6eef-4072-965a-713a1c185de1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "6495aa42-86a7-4541-b6ba-87a057b452ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d614d62c-6eef-4072-965a-713a1c185de1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49cdf68f-e90e-447a-adb6-331ec6be4b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d614d62c-6eef-4072-965a-713a1c185de1",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        },
        {
            "id": "02f2eb71-7839-459b-b8c8-ff9f49c5b081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "7c932667-1f73-43c2-920c-9da0edead292",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f2eb71-7839-459b-b8c8-ff9f49c5b081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9287663f-051d-45a4-916a-f5a8744a967e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f2eb71-7839-459b-b8c8-ff9f49c5b081",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        },
        {
            "id": "07bf0239-441d-4420-a905-88f16cadf8dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "888ca100-2edf-49e9-bc16-f3301b2daadd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07bf0239-441d-4420-a905-88f16cadf8dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebeed955-bb2c-42f1-b7bd-0bc7cd456d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07bf0239-441d-4420-a905-88f16cadf8dd",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        },
        {
            "id": "dd3d7c49-8a75-4bec-a893-db748fa043e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "compositeImage": {
                "id": "c7c708b6-1dc7-41a0-9ea3-8a19d8df5330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd3d7c49-8a75-4bec-a893-db748fa043e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08408847-1d62-48ef-9f85-bc80908fa147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd3d7c49-8a75-4bec-a893-db748fa043e6",
                    "LayerId": "a5e22e48-4d8f-49cf-a974-fa17af69069e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a5e22e48-4d8f-49cf-a974-fa17af69069e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}