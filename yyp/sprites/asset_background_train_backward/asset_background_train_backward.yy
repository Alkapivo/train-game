{
    "id": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_background_train_backward",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f907557e-4166-44b9-860d-da5f3d3bf598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "102c106b-937c-467b-85b3-86d4777f5e1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f907557e-4166-44b9-860d-da5f3d3bf598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3ff202e-ce3c-4278-a82f-7f42b63dd63c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f907557e-4166-44b9-860d-da5f3d3bf598",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "163ed53f-cbd3-41fa-b352-3716c3c184d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "ea18b3f7-0357-4fe6-af9c-c9f786f083b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163ed53f-cbd3-41fa-b352-3716c3c184d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acaf22cf-f534-496c-90d7-5ebb7160b147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163ed53f-cbd3-41fa-b352-3716c3c184d0",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "7de77572-3601-474e-93b4-2630b13be4ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "ee3206ba-b3c5-4447-929e-c71d974b6533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de77572-3601-474e-93b4-2630b13be4ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b132f0-3df8-448e-9a34-f696255b213e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de77572-3601-474e-93b4-2630b13be4ff",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "bfa9ef81-c75c-4315-b8c5-9c238ee90dd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "1cca1ff1-e433-47fb-b124-bf5f77305d66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa9ef81-c75c-4315-b8c5-9c238ee90dd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd32bb0e-7e78-4790-b4dc-d1303c0ed388",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa9ef81-c75c-4315-b8c5-9c238ee90dd8",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "b096b3d4-75ef-4be7-b878-10afb91c81ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "1c195f68-891c-4dda-841a-352c02ca13ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b096b3d4-75ef-4be7-b878-10afb91c81ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfbfc3be-a3fa-458c-830a-e9bd7f84be78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b096b3d4-75ef-4be7-b878-10afb91c81ec",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "8620cee8-f2bb-4e9b-a566-aa1317c1c0dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "95fe43cd-7e40-48e6-b7e8-94f056a985ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8620cee8-f2bb-4e9b-a566-aa1317c1c0dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91bd3a7a-40e3-4a9a-8a8c-855afd44ffab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8620cee8-f2bb-4e9b-a566-aa1317c1c0dd",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "6427159a-0fdf-4684-80de-339a4ae34256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "e835bff3-e67a-487f-adc8-13101516c7fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6427159a-0fdf-4684-80de-339a4ae34256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba3d101-a393-44b8-b9d6-75295939cfab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6427159a-0fdf-4684-80de-339a4ae34256",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "aa7ae7f5-3cf3-4ced-aeb2-8029d3b2ee10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "15cb3d8d-597b-4652-ab05-45da2c06b61b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7ae7f5-3cf3-4ced-aeb2-8029d3b2ee10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce377e9c-4da4-44d1-9998-a829d96be86e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7ae7f5-3cf3-4ced-aeb2-8029d3b2ee10",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "25b8b23c-7d0d-407b-860c-377818709c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "d3b219c8-0775-41ed-b3d4-2631241137ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b8b23c-7d0d-407b-860c-377818709c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "622a7a3b-864e-43bf-aa3d-4019a42bca23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b8b23c-7d0d-407b-860c-377818709c05",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "36b72394-65c1-48d4-9c12-88ab2c16bf07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "bf9f37ff-5ed8-4a2b-871e-26e5901f2bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36b72394-65c1-48d4-9c12-88ab2c16bf07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8ffe120-1d89-4586-9df5-b16cb9a2ea86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36b72394-65c1-48d4-9c12-88ab2c16bf07",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "3c9edc7d-a0e1-4c7e-8374-9d63cdc6468c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "eac8a167-5551-4ada-b372-4d9b011fa07c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c9edc7d-a0e1-4c7e-8374-9d63cdc6468c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed6f6689-944f-4d58-986a-32d1c6b4fae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c9edc7d-a0e1-4c7e-8374-9d63cdc6468c",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "c4f2c80f-b6fe-413f-b6b1-95c414a5656b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "b62ff813-7606-4e25-8a06-adc7fe889090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4f2c80f-b6fe-413f-b6b1-95c414a5656b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48c6e04e-e61e-4f93-991f-8e532187d43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4f2c80f-b6fe-413f-b6b1-95c414a5656b",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "b6a74d39-13e3-421d-a3be-8673adfccc1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "0015d576-f883-41e7-87d3-7467d5d0e092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6a74d39-13e3-421d-a3be-8673adfccc1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6705b00b-c7b2-4a1d-9a62-f450ed14f905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6a74d39-13e3-421d-a3be-8673adfccc1d",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "c015aed8-4031-4563-a649-5ab2f3b3ca27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "1d841389-013d-4a1b-b33b-93ae17f55a34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c015aed8-4031-4563-a649-5ab2f3b3ca27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "021a4ba2-c894-4d43-998c-84e238e09e87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c015aed8-4031-4563-a649-5ab2f3b3ca27",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "1001dbbe-7a75-4712-a15a-6a0c48969eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "416d7e6a-a125-4595-b8d9-130b51fc61fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1001dbbe-7a75-4712-a15a-6a0c48969eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3accd7e2-d6cf-4b93-8c7a-d35a0cca3bd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1001dbbe-7a75-4712-a15a-6a0c48969eb3",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "57a1fe0b-50c6-41c1-9e5d-3cc7b4f3d7d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "bc75f582-300a-401d-9bf1-0835b76a997b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a1fe0b-50c6-41c1-9e5d-3cc7b4f3d7d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7018fcf8-dc41-4fe5-baea-ac918f8f127c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a1fe0b-50c6-41c1-9e5d-3cc7b4f3d7d9",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "5ab82d1a-a6e0-4757-a746-2a140a3f006e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "34a2c018-1e7b-446d-9dd0-e8dea78b32dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab82d1a-a6e0-4757-a746-2a140a3f006e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04566f90-8e48-46c0-9cec-5c91f6010f03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab82d1a-a6e0-4757-a746-2a140a3f006e",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "72c834e3-e018-44e6-803c-aac2b3662c6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "a9993f70-87aa-4686-b388-4d58cd13bb9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c834e3-e018-44e6-803c-aac2b3662c6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb684712-02bf-46f7-a797-3d60c74cca19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c834e3-e018-44e6-803c-aac2b3662c6d",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "a3d95756-0464-4eb8-81b4-db8b8c450c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "aeff896d-4928-4023-a13c-20cb5054bc11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3d95756-0464-4eb8-81b4-db8b8c450c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22b73eb-edab-47b8-97e7-73aaf56d85a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3d95756-0464-4eb8-81b4-db8b8c450c17",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "71cc7c9f-c6fb-4821-82c8-d31a580454ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "a8c7c508-9781-4e87-adc7-b94867564cf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71cc7c9f-c6fb-4821-82c8-d31a580454ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b67f0a-0037-4d2d-8cda-16541a942a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71cc7c9f-c6fb-4821-82c8-d31a580454ef",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "7a0ee591-6974-420c-a0c2-bdf94ec8fbe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "d2c1e6c3-637c-456d-8c6c-bb721f41781e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a0ee591-6974-420c-a0c2-bdf94ec8fbe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3a0901-8380-4797-b127-4eb23cd3adce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a0ee591-6974-420c-a0c2-bdf94ec8fbe8",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "ab2f9daa-83fb-4772-9ccd-39340d7cbf29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "0ed061e3-8793-4e97-b3e1-436d077d37a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab2f9daa-83fb-4772-9ccd-39340d7cbf29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9372f0d-9e92-4c4d-8486-6be1110fe359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab2f9daa-83fb-4772-9ccd-39340d7cbf29",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "82f9f8d6-98be-4a08-8a38-2dbbe7d518f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "2b28b5ad-f869-4121-83d8-eb39c94cba9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f9f8d6-98be-4a08-8a38-2dbbe7d518f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b720f735-1380-450d-9170-3d077305bb2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f9f8d6-98be-4a08-8a38-2dbbe7d518f3",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "23a74770-333c-4078-9b8f-2c4748fe073a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "eba6b75c-2bea-4140-8b38-e604284da384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a74770-333c-4078-9b8f-2c4748fe073a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4d6e6ae-e1b0-4a67-ab3e-17b89f71d796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a74770-333c-4078-9b8f-2c4748fe073a",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "9d55b838-97c7-4a4b-9c4a-b9f5ccd55616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "84ecdceb-a243-4205-86d7-30cb70ce93c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d55b838-97c7-4a4b-9c4a-b9f5ccd55616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b33ba8-68ee-4b1d-abec-9babdb00481c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d55b838-97c7-4a4b-9c4a-b9f5ccd55616",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "72181a35-b15b-41ea-a8a9-27f19c332010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "85c0fbea-9091-4ddd-9a6f-64bdf61f7952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72181a35-b15b-41ea-a8a9-27f19c332010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0f089b4-e625-4717-a974-ca4591f1cc88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72181a35-b15b-41ea-a8a9-27f19c332010",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "e9d306ab-e560-479c-8082-f9ae05c7db03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "2569181d-243c-4dd7-aacb-1955c135a9e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d306ab-e560-479c-8082-f9ae05c7db03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d2d18e-3f78-4b81-8e15-b296802cba10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d306ab-e560-479c-8082-f9ae05c7db03",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "62e5bf2e-b77c-45b7-b94f-7c9bb6ed5dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "e3b64732-6f29-4dc2-9671-ce683d12be3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e5bf2e-b77c-45b7-b94f-7c9bb6ed5dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd5ce22b-f329-4a28-a226-7d674ed83a48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e5bf2e-b77c-45b7-b94f-7c9bb6ed5dfb",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "a493b76d-d1ce-4246-aed7-d7d74c85a0a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "5991d273-02ca-41f0-8ac4-a35ad98229f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a493b76d-d1ce-4246-aed7-d7d74c85a0a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a2f608-5b39-4c48-80c9-053e14206a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a493b76d-d1ce-4246-aed7-d7d74c85a0a2",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "22b2c0ff-caaf-4cfa-a6fc-06d11f1ba0e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "958e850a-68b9-4ab5-807e-79e3b92012db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22b2c0ff-caaf-4cfa-a6fc-06d11f1ba0e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d41890d-931e-4ce8-9bbc-415977a4b0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22b2c0ff-caaf-4cfa-a6fc-06d11f1ba0e4",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "3c2d0a4e-4905-4fe6-a75e-92ffb8c33f6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "4687de43-a7d3-432e-b669-2b56a4683330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c2d0a4e-4905-4fe6-a75e-92ffb8c33f6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b612a94-8233-4d73-beae-5fe7e2cd4a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c2d0a4e-4905-4fe6-a75e-92ffb8c33f6c",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "d01ff137-09e6-42c1-b03b-564ccf8e0d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "a664583d-047e-411e-9206-12dc8dd20122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01ff137-09e6-42c1-b03b-564ccf8e0d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64244d2c-5711-400b-8a2c-5cacc02218d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01ff137-09e6-42c1-b03b-564ccf8e0d83",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "5a75d3e1-40ac-4de9-9cca-afdf805d4eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "528ad858-f9f3-402f-9512-feddce173a09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a75d3e1-40ac-4de9-9cca-afdf805d4eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1cc5011-7600-4194-97ff-0b68832dfe13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a75d3e1-40ac-4de9-9cca-afdf805d4eef",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "33c48cb4-4fb1-406b-a67b-23d369f25224",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "f9fd33c9-4927-4d51-89ec-6095917817b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c48cb4-4fb1-406b-a67b-23d369f25224",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa67f781-b7d2-471f-aff5-b61fee1845a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c48cb4-4fb1-406b-a67b-23d369f25224",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "ca4b1f95-280f-4c57-91db-f65e31890fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "b0beb64a-55cf-4a61-b10b-94666aeb98ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca4b1f95-280f-4c57-91db-f65e31890fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db21f9e8-8ef8-49bc-a0c3-43c473696161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca4b1f95-280f-4c57-91db-f65e31890fa0",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "2f7c6542-a716-4b89-abff-bf6dc686d334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "786f6203-79d8-4e6f-9550-7ade0d0c5e7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7c6542-a716-4b89-abff-bf6dc686d334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ec87a1d-4e28-4fcf-a273-4bfecd20b444",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7c6542-a716-4b89-abff-bf6dc686d334",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "51ff96e8-fad3-4c92-86a5-4a91fab8e1e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "d72c14c3-18ff-4eaf-a99f-f17a0087e692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ff96e8-fad3-4c92-86a5-4a91fab8e1e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27a7ce50-77a5-42dd-83bd-58a290637706",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ff96e8-fad3-4c92-86a5-4a91fab8e1e5",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "d087e4cb-dafa-4179-9c08-46200f2f5cf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "9affa2ac-9d46-497e-a2e1-999bd4328004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d087e4cb-dafa-4179-9c08-46200f2f5cf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37554602-3bc3-4cea-b968-52bec771ccc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d087e4cb-dafa-4179-9c08-46200f2f5cf9",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "1646c020-090b-4d46-a29e-e3817adc06bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "f667dff8-7885-4438-bd3b-a8421dfcad58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1646c020-090b-4d46-a29e-e3817adc06bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2986606b-2afd-44fc-a229-200c8de6824e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1646c020-090b-4d46-a29e-e3817adc06bf",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "ed926721-c6a3-45f2-9320-6ca7077eda7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "2189d32b-48ce-4357-8283-1e668a23b5bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed926721-c6a3-45f2-9320-6ca7077eda7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db665da-05a8-4283-9600-3d3afc821602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed926721-c6a3-45f2-9320-6ca7077eda7c",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "dbd566ac-b6ec-4db2-a355-5dcdca159e2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "9af4f752-bdd3-42e9-b834-483cc209c25a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbd566ac-b6ec-4db2-a355-5dcdca159e2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91194ed7-cec0-4152-8bd2-0b83a3a56a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbd566ac-b6ec-4db2-a355-5dcdca159e2c",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "239ab6d4-d0d4-4bab-868b-d93b9e63178d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "a13f5a80-5179-4042-8c56-34677490ef49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "239ab6d4-d0d4-4bab-868b-d93b9e63178d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5feb1c5-fdb4-468b-a7c6-319d95f972f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "239ab6d4-d0d4-4bab-868b-d93b9e63178d",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "092e37e5-5039-44e7-8efd-69c141f39481",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "bc3b79d9-3d4c-45cb-87dc-ef0477a6a360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "092e37e5-5039-44e7-8efd-69c141f39481",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d91b3886-9fe1-46c9-989a-fd71eb9fd77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "092e37e5-5039-44e7-8efd-69c141f39481",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "9efee3dd-b98a-4a63-b60c-c5730114d943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "752b5686-8c38-4064-bd9a-8706da6a3ac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9efee3dd-b98a-4a63-b60c-c5730114d943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c0c6d3-79d9-4ff0-8074-dfa679a5dbb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9efee3dd-b98a-4a63-b60c-c5730114d943",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "d55db352-0444-45d8-9c26-b58ff2ed60f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "075def80-502c-4bfb-af6f-a06a63439ecb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d55db352-0444-45d8-9c26-b58ff2ed60f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93a40785-6423-4744-99d2-a445c009b18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d55db352-0444-45d8-9c26-b58ff2ed60f5",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "a1b208bc-0e8a-47de-9ddb-a3cd436e36b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "57b1bb50-6434-4b6d-9f24-7be087e73471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1b208bc-0e8a-47de-9ddb-a3cd436e36b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff37d682-aa6e-448b-8ee2-21d08e06f5c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1b208bc-0e8a-47de-9ddb-a3cd436e36b5",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "97a40a8c-c110-43c7-989c-983c562c0e80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "a2cb17a3-2035-4a5a-af37-08d01811c4e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a40a8c-c110-43c7-989c-983c562c0e80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae71fdf7-a170-4a33-96a2-f8d83123339a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a40a8c-c110-43c7-989c-983c562c0e80",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "38ab295d-ff06-44a5-a2d0-7012a1b11369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "172b9bf0-a4cd-4ee9-99d9-5c7210a7a8b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ab295d-ff06-44a5-a2d0-7012a1b11369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a949a7de-8378-4c00-bf8d-121a50cf353d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ab295d-ff06-44a5-a2d0-7012a1b11369",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "8d1b3e68-58d5-40f0-9535-b0d9e70ed0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "e9fee90d-6b67-46fa-83d0-ed1e7574d30e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d1b3e68-58d5-40f0-9535-b0d9e70ed0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb83a8c-3814-4c03-9f67-8f4424d24d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d1b3e68-58d5-40f0-9535-b0d9e70ed0e6",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        },
        {
            "id": "c5729649-6ead-4c3f-8e7f-bdd8089c206e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "compositeImage": {
                "id": "86761fc5-f3e7-402c-99f2-e6bd13d45ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5729649-6ead-4c3f-8e7f-bdd8089c206e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "634a56e8-b969-4f88-8cfa-8d48505513f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5729649-6ead-4c3f-8e7f-bdd8089c206e",
                    "LayerId": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "873ed4aa-d2b1-410d-b1f6-1cfe04484c80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e676cd02-9e5f-4cda-8052-c0fe63b1a88e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 160
}