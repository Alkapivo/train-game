{
    "id": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_background_train_forward",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d569355d-055c-4f8a-923c-f5c48b3d5d2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "9b3acf6a-bf23-4a48-90fa-8a0bd1a56f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d569355d-055c-4f8a-923c-f5c48b3d5d2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1188f03-253b-4d0a-84e6-392f65fec3fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d569355d-055c-4f8a-923c-f5c48b3d5d2d",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "38740c93-673f-4028-a48d-10b6c74c0628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "0dab25ab-65ff-46d3-a43c-6bb509f98c64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38740c93-673f-4028-a48d-10b6c74c0628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f74ef737-a21f-4771-afb6-4a5349aa9a29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38740c93-673f-4028-a48d-10b6c74c0628",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "bc208c2c-f8af-46b7-8930-ef5b2edc7f02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "5abcd3e3-a25f-4b2f-88f1-c8657238a08f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc208c2c-f8af-46b7-8930-ef5b2edc7f02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b4a4383-cb98-4cb0-acbe-d356d1192010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc208c2c-f8af-46b7-8930-ef5b2edc7f02",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "c530ae80-b1ca-4a71-996b-94ef7eaf835a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "95f42778-3627-4f61-b298-e0c4e9a524c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c530ae80-b1ca-4a71-996b-94ef7eaf835a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b03dde-b4a2-4bb9-bb60-3d92ef19d8eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c530ae80-b1ca-4a71-996b-94ef7eaf835a",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "a6747ec5-86cf-4535-97c4-0b4f64e0d47c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "af8ad183-7520-43ea-b984-c1b4e14464d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6747ec5-86cf-4535-97c4-0b4f64e0d47c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9424920-c17d-4b4a-9482-17052f4f6540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6747ec5-86cf-4535-97c4-0b4f64e0d47c",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "64b00d54-46b1-420a-a5b2-c2894780546e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "dbd21932-5495-4376-9856-67aa641b7e49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64b00d54-46b1-420a-a5b2-c2894780546e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a70030fd-939d-4552-9a8a-603d1e846632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64b00d54-46b1-420a-a5b2-c2894780546e",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "b8772092-069f-4857-b430-db496f77655d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "7abdb021-98c7-4913-8ab3-755a24bb1bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8772092-069f-4857-b430-db496f77655d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2870eb06-8cfc-4b6f-b4d4-22b3b3a3cda5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8772092-069f-4857-b430-db496f77655d",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "c06570a2-5284-4ced-8fcb-f3aece4d638b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "b297f9a4-cb0e-4fac-8029-73c7c6a285f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c06570a2-5284-4ced-8fcb-f3aece4d638b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a128eb-7d95-4034-bec1-31cbf39abd07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c06570a2-5284-4ced-8fcb-f3aece4d638b",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "34617ce7-b643-4805-ab84-e897284c8d02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "9e76515a-0bc7-44bf-8e35-96ecae9eb7bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34617ce7-b643-4805-ab84-e897284c8d02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8531410f-fedd-45e1-b160-72e50847a82d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34617ce7-b643-4805-ab84-e897284c8d02",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "f14223a1-3384-417f-9240-1bca131926b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "37372956-8d9b-4fc9-82b0-3be53c295d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f14223a1-3384-417f-9240-1bca131926b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb69425-52ea-4160-9971-9a29f1083edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f14223a1-3384-417f-9240-1bca131926b4",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "f4a58ecd-5d82-4232-b916-4f0cbc038e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "4ee89b50-0292-4dad-82cc-e88cd47603a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a58ecd-5d82-4232-b916-4f0cbc038e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a279d674-7a6f-492e-a8a1-8d7646c96255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a58ecd-5d82-4232-b916-4f0cbc038e39",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "4cf743bc-701e-4341-976c-96ce2b0dc95f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "27f32942-b05c-4468-bab0-57d120b137a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf743bc-701e-4341-976c-96ce2b0dc95f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d693961-31f1-4af3-a71b-19d08bf3407d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf743bc-701e-4341-976c-96ce2b0dc95f",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "f19ee37e-a17f-4b32-be71-d2b8828ef41e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "828617c7-8ad2-4289-8e5e-a071f31adeb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f19ee37e-a17f-4b32-be71-d2b8828ef41e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b418d7bd-b534-4c0d-b11d-46a41c50026c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f19ee37e-a17f-4b32-be71-d2b8828ef41e",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "ad7ad158-f5f4-4297-b950-aba6112f6c29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "3b2fcb70-b632-42bf-a6be-965865f4e810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad7ad158-f5f4-4297-b950-aba6112f6c29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aed75a1-e385-44db-b3eb-ed6ebd3d5992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad7ad158-f5f4-4297-b950-aba6112f6c29",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "68a6fd0d-00cf-4ffa-8114-0b6f19ba2000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "815bc91c-579d-407f-8d51-f76830761571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68a6fd0d-00cf-4ffa-8114-0b6f19ba2000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e006f4de-906a-4a7b-bb9c-060671d5373c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68a6fd0d-00cf-4ffa-8114-0b6f19ba2000",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "9ba9dd88-8f9b-4c47-8810-e8bfbf60d740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "45c587a6-861e-4e88-a4a7-a40877285c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba9dd88-8f9b-4c47-8810-e8bfbf60d740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220edeea-df90-4e19-85f0-a3b03f450fdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba9dd88-8f9b-4c47-8810-e8bfbf60d740",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "5344d3a5-c223-4ead-93ce-84d2f5bd08ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "f6885dfb-92aa-4528-9b04-671301ea88b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5344d3a5-c223-4ead-93ce-84d2f5bd08ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de68babe-1829-43e8-a2bc-c3c496e856dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5344d3a5-c223-4ead-93ce-84d2f5bd08ab",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "614ea7be-4891-421d-8bce-8100e0f7756a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "f665e905-774f-4183-be0f-6757daa1d336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614ea7be-4891-421d-8bce-8100e0f7756a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "035f5d66-c501-4ac8-a9e7-20cc88893683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614ea7be-4891-421d-8bce-8100e0f7756a",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "498582d8-3903-4f1a-9ffb-29643a99b381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "37dffefd-1e72-4ab3-9d38-1159acf0b44d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "498582d8-3903-4f1a-9ffb-29643a99b381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5c6b599-b9fe-4f68-92e9-7adeff844b8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "498582d8-3903-4f1a-9ffb-29643a99b381",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "bcbde5e0-3a6b-4845-921a-dbe4e8f25940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "ee7aac14-41ad-48e3-a6c1-11e33633f32e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcbde5e0-3a6b-4845-921a-dbe4e8f25940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4d9467-9cee-42af-b500-c607cb9a7af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcbde5e0-3a6b-4845-921a-dbe4e8f25940",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "80f1f58b-f5da-4919-b5d6-46d35c257ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "12282a3a-58d7-40fc-80e6-b70590abfd9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80f1f58b-f5da-4919-b5d6-46d35c257ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "635a65a4-8b1b-49a4-8c6b-73a7b22e9459",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80f1f58b-f5da-4919-b5d6-46d35c257ba3",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "ccbd7efd-fd65-45a2-8f86-67665f28c623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "9ed1e76d-2bca-45ae-abaa-ad061262bcd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccbd7efd-fd65-45a2-8f86-67665f28c623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482cc2c7-ddd3-4c7c-a14e-af9259d9ac9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccbd7efd-fd65-45a2-8f86-67665f28c623",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "30897ac8-6007-4ecf-87f4-9c01eb3fbf05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "aa66bb2c-cd47-44e4-95e8-3c5ffadc018a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30897ac8-6007-4ecf-87f4-9c01eb3fbf05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3e7983-95dc-4d20-a943-71f8cb81a621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30897ac8-6007-4ecf-87f4-9c01eb3fbf05",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "61085eaa-aafb-47d5-b1a6-c4b194c1bbb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "34278a47-c3dc-4994-acea-eb6b83f66109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61085eaa-aafb-47d5-b1a6-c4b194c1bbb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f8f9429-e43b-4940-9977-b503adae3b77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61085eaa-aafb-47d5-b1a6-c4b194c1bbb6",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "4c78d97d-b513-43e0-8740-7030b89218b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "1b83c51e-7f79-4d8e-a563-1a92908d3c03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c78d97d-b513-43e0-8740-7030b89218b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04e49474-101b-4e52-bda0-3f63a2c0ae05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c78d97d-b513-43e0-8740-7030b89218b9",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "f92a22e6-a95f-4000-bac9-6263e5be1113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "d7a8e2da-a7ab-40f3-b6a3-cac4d0ad1fd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f92a22e6-a95f-4000-bac9-6263e5be1113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3cc7087-ab9b-456c-913f-a75a7f5e27c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f92a22e6-a95f-4000-bac9-6263e5be1113",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "e0e77516-0a9e-4354-9f64-96482e10bfe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "aff7eace-cdba-4fee-981b-5aad809b7699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0e77516-0a9e-4354-9f64-96482e10bfe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5880bebc-3f6a-4566-aca2-e25bbd86b600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0e77516-0a9e-4354-9f64-96482e10bfe3",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "dcc742a5-15b0-4890-ad46-8266292d74c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "b1a0f11a-21ba-4686-800e-4e8b95a2c336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcc742a5-15b0-4890-ad46-8266292d74c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1befc732-6934-425f-a35c-d8383411e076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcc742a5-15b0-4890-ad46-8266292d74c6",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "c2d76e13-c7fe-4c85-84e6-95ec4c62b1ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "d44ae51d-5ed9-4a9d-9ba0-043f0515230a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d76e13-c7fe-4c85-84e6-95ec4c62b1ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf1962f-7c9f-4fdd-b1ee-5e42c3739d99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d76e13-c7fe-4c85-84e6-95ec4c62b1ea",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "8907dd8c-f19c-4a6d-94d6-1cf69423857c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "f9262daa-261c-4570-9029-5bb38044005f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8907dd8c-f19c-4a6d-94d6-1cf69423857c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e74e9227-f73f-4c78-8837-afad798568cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8907dd8c-f19c-4a6d-94d6-1cf69423857c",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "e3b664af-d6a5-4f81-b1b6-52645cf0814e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "3f6ae57b-31e3-4785-b569-f97ce2a5c00a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b664af-d6a5-4f81-b1b6-52645cf0814e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed2822d4-8b89-4878-ac9f-89639a4a4a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b664af-d6a5-4f81-b1b6-52645cf0814e",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "96cf2894-d0fc-49fc-a127-ac74ff98b2d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "b2fec027-4ea0-4b9b-b29d-78ae9951e028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96cf2894-d0fc-49fc-a127-ac74ff98b2d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7465034-8218-4d2d-aff7-a18e1cb661bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96cf2894-d0fc-49fc-a127-ac74ff98b2d3",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "5426a33c-4a06-49b4-bd26-0202e55291d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "0dc4e224-1ed1-44c6-9e11-ebc51a485527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5426a33c-4a06-49b4-bd26-0202e55291d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce308ceb-1417-481f-88e8-9a05f5399036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5426a33c-4a06-49b4-bd26-0202e55291d1",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "e76d205e-844b-4ab7-ae70-c82f4b422eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "fd48269f-5996-4395-af0b-3ae22b31d99f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e76d205e-844b-4ab7-ae70-c82f4b422eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9fe4f48-1dff-457b-85e0-7dbf618e0fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e76d205e-844b-4ab7-ae70-c82f4b422eef",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "a756cfce-ce4e-4389-b742-387acc795170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "2a2fdd85-f07e-41e8-8dc6-1d7472414569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a756cfce-ce4e-4389-b742-387acc795170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "457f9177-12a8-4b63-83a2-60e60839603c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a756cfce-ce4e-4389-b742-387acc795170",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "d73a21f2-890e-4d4e-a557-31b24934c958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "54b8aad9-e8b1-4057-9d74-7ce74ecf0b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d73a21f2-890e-4d4e-a557-31b24934c958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab4023e9-0b07-4fb5-ba3d-9ac3efa69104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d73a21f2-890e-4d4e-a557-31b24934c958",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "3ca8bb7f-4ba0-4a28-a2ae-831e1616d378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "22b3e976-5d65-42d8-b540-4e97d5b4948f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ca8bb7f-4ba0-4a28-a2ae-831e1616d378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba380b5-037c-464d-bcab-5b81d8ed88ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ca8bb7f-4ba0-4a28-a2ae-831e1616d378",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "8ee63d54-7d0d-4574-896d-e30e66a4e8d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "b4104d97-15f0-495d-910a-e7bf1a4aada3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ee63d54-7d0d-4574-896d-e30e66a4e8d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1285fad-57c6-4afc-a68d-b5b679c483da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ee63d54-7d0d-4574-896d-e30e66a4e8d1",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "635072c1-913d-41fb-9a7c-df86abc0f8bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "7247fafc-db4f-4ccf-8107-8ff4d3fedf7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "635072c1-913d-41fb-9a7c-df86abc0f8bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26d576c6-8afc-481e-872c-3cb804fcca40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "635072c1-913d-41fb-9a7c-df86abc0f8bd",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "362e60d6-92a5-448e-b111-3c9bea313697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "1eb494e7-09be-462c-9f81-7d719bd26b38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362e60d6-92a5-448e-b111-3c9bea313697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e19af1-c177-47cf-b6e5-d27da5353ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362e60d6-92a5-448e-b111-3c9bea313697",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "210e9102-18de-44e0-a291-1e677e4c1fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "acdba650-60f5-42c2-ae72-cc683c25a412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "210e9102-18de-44e0-a291-1e677e4c1fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f73c0c8b-618b-43a5-a6fa-6b0120d86e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "210e9102-18de-44e0-a291-1e677e4c1fa0",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "95b3658b-bbfa-477a-bcb8-bea6292dd128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "10070b40-66b5-4e2d-978b-1b5b77226ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b3658b-bbfa-477a-bcb8-bea6292dd128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2751ba10-ea08-4cdf-b195-46033fe5ace9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b3658b-bbfa-477a-bcb8-bea6292dd128",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "cf07eb49-8de8-4e9e-965e-20afbc0aea31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "5a8fc628-f669-4d60-8111-e93f70ba8ca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf07eb49-8de8-4e9e-965e-20afbc0aea31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9377f18a-2c6f-4961-8313-d20961f132ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf07eb49-8de8-4e9e-965e-20afbc0aea31",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "7eaa9081-e42d-4336-ba18-48be39454786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "ee84e459-67b2-4d34-b00a-43a99e2b0706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eaa9081-e42d-4336-ba18-48be39454786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79e76b1-ce3e-4aab-a3ea-5d24becf58b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eaa9081-e42d-4336-ba18-48be39454786",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "a2bdbd46-2e9a-4413-8659-76506bdcd25c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "76f65de1-51f7-4e06-a246-ce1224edf136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2bdbd46-2e9a-4413-8659-76506bdcd25c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9546579a-29a9-4759-9476-a0f44929755f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2bdbd46-2e9a-4413-8659-76506bdcd25c",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "cb7606f1-1e06-46e6-b59d-90fe8b94c128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "d0afe9d6-c11a-4c79-bd6e-603eb1d995af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7606f1-1e06-46e6-b59d-90fe8b94c128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "696384cd-9f55-4657-b91c-678c53249ac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7606f1-1e06-46e6-b59d-90fe8b94c128",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "fdcf3a14-dc3a-4322-9740-ae1a21923837",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "cc22c2b0-a21c-4a3d-9a32-0647d1160346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdcf3a14-dc3a-4322-9740-ae1a21923837",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96728b1f-6f8e-4b81-8236-eb96afab6b7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdcf3a14-dc3a-4322-9740-ae1a21923837",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "d8b8e09e-332e-49ce-a7fc-ee46a474c6c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "d8ba159c-2bd2-435f-b066-abc4465ca3e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b8e09e-332e-49ce-a7fc-ee46a474c6c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29078b8d-374b-4b1f-bd01-5f65fdbfdea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b8e09e-332e-49ce-a7fc-ee46a474c6c3",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "1c10fee5-729b-4e03-ac21-a185a4e8a12f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "2cfea9ce-79d4-4018-9398-6742070a165f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c10fee5-729b-4e03-ac21-a185a4e8a12f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7f5052-e6ca-401c-abd1-0f9973a01b58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c10fee5-729b-4e03-ac21-a185a4e8a12f",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        },
        {
            "id": "e1c49420-43ff-455d-bff4-3e65899f24b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "compositeImage": {
                "id": "c81a8076-2c84-4ce8-bfd8-0848dd0d8825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c49420-43ff-455d-bff4-3e65899f24b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1236c912-f342-4a8c-86dd-de44fdb57782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c49420-43ff-455d-bff4-3e65899f24b8",
                    "LayerId": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "9cf3ddbe-a65f-4b89-a19c-d2ee3941df5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a21b0abd-0a00-4e34-aab9-7cefa188e59b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 160
}