{
    "id": "7d9cc81b-78f3-4be7-8e8e-8f642cc775bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_avatar_bugz",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2629a3be-bc50-4023-bb3e-1694d40caa31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9cc81b-78f3-4be7-8e8e-8f642cc775bb",
            "compositeImage": {
                "id": "ffe50163-d7c9-4fa7-b6f2-63e027120a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2629a3be-bc50-4023-bb3e-1694d40caa31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb596da-6c31-49fb-ae2c-d0b3a1ae1daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2629a3be-bc50-4023-bb3e-1694d40caa31",
                    "LayerId": "0083e7eb-8fed-4266-8ae3-55b06b39a97d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 251,
    "layers": [
        {
            "id": "0083e7eb-8fed-4266-8ae3-55b06b39a97d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d9cc81b-78f3-4be7-8e8e-8f642cc775bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 250
}