{
    "id": "2ca09093-3dff-42d0-917b-244a240e73fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_shroom_02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 252,
    "bbox_left": 15,
    "bbox_right": 242,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cb10e12-345e-4d52-a3f1-683b8bf8a4dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ca09093-3dff-42d0-917b-244a240e73fe",
            "compositeImage": {
                "id": "d6e9d014-5464-48dd-bd5f-f685c6537ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb10e12-345e-4d52-a3f1-683b8bf8a4dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b2d035-a407-4d79-b10b-39be5b1b9347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb10e12-345e-4d52-a3f1-683b8bf8a4dc",
                    "LayerId": "3e500038-ca7b-4b5c-a218-6fbc889764b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "3e500038-ca7b-4b5c-a218-6fbc889764b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ca09093-3dff-42d0-917b-244a240e73fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}