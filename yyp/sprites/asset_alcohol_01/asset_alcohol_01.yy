{
    "id": "f2d82fa8-ad5b-47de-a776-316975fc252a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "asset_alcohol_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 540,
    "bbox_left": 204,
    "bbox_right": 342,
    "bbox_top": 272,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87490f1d-551d-437d-9e90-63d854ecf4ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2d82fa8-ad5b-47de-a776-316975fc252a",
            "compositeImage": {
                "id": "6554756e-9636-4ace-bdf2-c6fe49ae5824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87490f1d-551d-437d-9e90-63d854ecf4ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d93b6a9-cf9d-471a-a9fd-481450ddd6ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87490f1d-551d-437d-9e90-63d854ecf4ce",
                    "LayerId": "233c9020-8621-47ab-80fb-81b60d5729bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "233c9020-8621-47ab-80fb-81b60d5729bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2d82fa8-ad5b-47de-a776-316975fc252a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}