{
    "id": "ba7038c8-c1a8-4663-bdb9-593cc3ff262f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Shroom",
    "eventList": [
        {
            "id": "0f264d4d-25b3-4d8d-8997-c7880c655fe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba7038c8-c1a8-4663-bdb9-593cc3ff262f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f220c86a-f81e-4d39-836d-d15c8adabe63",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0b33b3e0-a0f2-479b-9fe4-e580fe015a84",
    "visible": true
}