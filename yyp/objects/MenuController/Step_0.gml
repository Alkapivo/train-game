///@description Input listener

	
	if (keyboard_check_pressed(vk_up)) {
		optionPointer = clamp(optionPointer - 1, 0, optionsLength - 1);
	}
	if (keyboard_check_pressed(vk_down)) {
		optionPointer = clamp(optionPointer + 1, 0, optionsLength - 1);
	}
	if (keyboard_check(vk_enter)) {
		global.pilotType = optionPointer;
		room_goto(SceneGame);
	}

	if (mouse_check_button(mb_left)) {
		for (var index = 0; index < optionsLength; index++) {
			var xBegin = (global.guiWidth / 2.0) - 50;
			var yBegin = ((global.guiHeight / 2.0) + index * textHeight) - 20;
			var xEnd = (global.guiWidth / 2.0) + 50;
			var yEnd = ((global.guiHeight / 2.0) + index * textHeight) + 20;
				
			var xMouse = window_view_mouse_get_x(view);
			var yMouse = window_view_mouse_get_y(view);
		
			if (point_in_rectangle(
				xMouse,
				yMouse,
				xBegin,
				yBegin,
				xEnd,
				yEnd)) {
			
				global.pilotType = index;
				room_goto(SceneGame);
			}
		}
	}