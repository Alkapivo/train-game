///@description Initialize
	
	#region Resolution
	view_enabled = true;
	window_set_fullscreen(global.isFullscreen);
	display_set_gui_size(global.guiWidth, global.guiHeight);
	application_surface_enable(true);
	surface_resize(application_surface, global.guiWidth, global.guiHeight);
	#endregion
	
	#region Fields
	textTheta = 0;
	textAngle = 0;
	textXScale = 2.0;
	textYScale = 2.0;
	textAngleModifier = 5.0;
	textHeight = 80;
	#endregion
	
	#region Camera
	view_enabled = true;
	angle = 0;
	camera = camera_create_view(0, 0, global.guiWidth, global.guiHeight, angle);
	view = 0;
	view_set_camera(view, camera);
	#endregion
	
	options = global.pilotTypes;
	optionPointer = 0;
	optionsLength = array_length_1d(options);

	