{
    "id": "b1d0b97b-7f07-423c-82a7-1c6ef24f6900",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "MenuController",
    "eventList": [
        {
            "id": "289cf27b-7a4c-4379-8084-77e79b5cf9ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1d0b97b-7f07-423c-82a7-1c6ef24f6900"
        },
        {
            "id": "5cf7b496-08c3-466a-b21a-55516d1fcb82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1d0b97b-7f07-423c-82a7-1c6ef24f6900"
        },
        {
            "id": "62f060a7-a026-44ba-ab63-9c0b1930d1f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b1d0b97b-7f07-423c-82a7-1c6ef24f6900"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}