///@description Text

	textTheta += 0.01;
	if (textTheta > pi * 2) {
		textTheta = 0;
	}
	
	textAngle++
	if (textAngle > 360) {
		textAngle = 0;	
	}

	draw_sprite_ext(
		asset_background_train_backward,
		image_index,
		global.guiWidth / 2.0,
		global.guiHeight / 2.0,
		2.0,
		2.0,
		0.0,
		c_fuchsia,
		1.0
	);


	draw_set_color(choose(
		c_white,
		c_fuchsia,
		c_lime));
	draw_set_alpha(1.0);
	draw_set_font(asset_font_title);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	
	draw_text_transformed(
		global.guiWidth / 2.0,
		global.guiHeight * 0.25,
		"Train game",
		choose(0.95, 0.96, 0.97, 0.98, 0.99, 1.0, 1.01, 1.02, 1.03, 1.04, 1.05),
		choose(0.95, 0.96, 0.97, 0.98, 0.99, 1.0, 1.01, 1.02, 1.03, 1.04, 1.05),
		0.0);
	
	draw_set_font(asset_font_consolas);
	for (var index = 0; index < optionsLength; index++) {
		
		var option = options[index];
		var isOptionSelected = os_type == os_android ? true : optionPointer == index;
		var optionText = option[PilotType.NAME];
		
		if (isOptionSelected) {
			draw_set_color(c_red);
			draw_set_alpha(0.5);
			draw_ellipse(
				(global.guiWidth / 2.0) - 50 - 30 * (index + 1 div 2 == 0 ? sin(textTheta) : cos(textTheta)),
				((global.guiHeight / 2.0) + index * textHeight) - 10 - 30 * cos(textTheta) * textYScale,
				(global.guiWidth / 2.0) + 50 + 30 * (index + 1 div 2 == 0 ? sin(textTheta) : cos(textTheta)),
				((global.guiHeight / 2.0) + index * textHeight) + 10 + 30 * cos(textTheta) * textYScale,
				false);
		}
		draw_set_color(choose(
			c_white,
			c_fuchsia,
			c_lime));
		draw_set_alpha(1.0);
		draw_text_transformed(
			global.guiWidth / 2.0,
			(global.guiHeight / 2.0) + index * textHeight,
			optionText,
			(index + 1 div 2 == 0 ? sin(textTheta) : cos(textTheta)) * textXScale,
			(index + 1 div 2 == 0 ? cos(textTheta) : sin(textTheta)) * textYScale,
			textAngle + sin(textTheta) * textAngleModifier);
	}
	
	