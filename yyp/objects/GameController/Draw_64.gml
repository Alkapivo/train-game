///@description Messages

	#region Camera effects
	if (cameraIsShaking) {
		cameraShake -= 0.2;
		camera_set_view_angle(camera, cos(cameraShake) * 8);
		if (cameraShake <= 0.0) {
			cameraIsShaking = 0;
			cameraShake = 0;
			camera_set_view_angle(camera, 0.0);
		}
	}
	#endregion

	for (var index = ds_list_size(messageList) - 1; index >= 0; index--) {
		
		draw_set_alpha(1.0);
		draw_set_font(asset_font_consolas);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		
		var messageTask = messageList[| index];
		var message = messageTask[1];
		
		var margin = 48;
		var textHeight = 48;
		var boxWidth = (string_width(message) / 2.0) * 1.6;
		var boxHeight = (string_height(message) / 2.0) * 1.6;
		draw_set_color(c_black);
		draw_set_alpha(0.6);
		draw_rectangle(
			(global.guiWidth / 2.0) - boxWidth,
			(margin + textHeight * index) - boxHeight,
			(global.guiWidth / 2.0) + boxWidth,
			(margin + textHeight * index) + boxHeight,
			false);
		draw_set_alpha(1.0);
	
		if (message = "OMG, DONT!!!") {
			draw_set_color(c_red);
			hitBlendColorEnable = true;
			if (!cameraIsShaking) {
				cameraIsShaking = true;
				cameraShake = pi * 8;
			}
		} else {
			hitBlendColorEnable = false;
			draw_set_color(choose(
				c_white,
				c_fuchsia,
				c_lime));			
		}


	
		draw_text_transformed(
			global.guiWidth / 2.0,
			margin + textHeight * index,
			message,
			choose(0.95, 0.96, 0.97, 0.98, 0.99, 1.0, 1.01, 1.02, 1.03, 1.04, 1.05),
			choose(0.95, 0.96, 0.97, 0.98, 0.99, 1.0, 1.01, 1.02, 1.03, 1.04, 1.05),
			0.0);
	}
	
	window_set_cursor(cr_none);
	
	angle = point_direction(
		global.guiWidth / 2.0,
		global.guiHeight / 2.0,
		window_view_mouse_get_x(view),
		window_view_mouse_get_y(view));
		
	draw_sprite_ext(
			asset_cursor,
			0,
			window_view_mouse_get_x(view),
			window_view_mouse_get_y(view),
			mouseChecked ? choose(2.4, 2.5) : 1.0,
			mouseChecked ? choose(2.4, 2.5) : 1.0,
			angle,
			mouseFreeze ? c_red : c_white,
			1.0);
	

	angle = point_direction(
		global.guiWidth / 2.0,
		global.guiHeight / 2.0,
		global.guiWidth - window_view_mouse_get_x(view),
		window_view_mouse_get_y(view));

	draw_sprite_ext(
			asset_cursor,
			0,
			global.guiWidth - window_view_mouse_get_x(view),
			window_view_mouse_get_y(view),
			mouseChecked ? choose(2.4, 2.5) : 1.0,
			mouseChecked ? choose(2.4, 2.5) : 1.0,
			angle,
			mouseFreeze ? c_red : c_white,
			1.0);
	
