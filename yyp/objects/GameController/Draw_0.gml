///@description Background

	spriteTheta += 0.1;
	if (spriteTheta > pi * 2) {
		spriteTheta = 0;	
	}
		
	var backgroundBlendColor = hitBlendColorEnable ? 
		hitBlendColor : 
		make_color_rgb(red, green, blue);
	draw_sprite_ext(
		asset_background_train_forward,
		image_index,
		global.guiWidth / 2.0,
		global.guiHeight / 2.0,
		2.0,
		2.0,
		0.0,
		backgroundBlendColor,
		1.0
	);