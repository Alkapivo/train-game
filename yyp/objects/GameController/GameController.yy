{
    "id": "088ad8c3-2e9c-4d59-afea-3a913d6c08fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "GameController",
    "eventList": [
        {
            "id": "01cc4a2d-6039-4ead-bb7a-318490e73853",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "088ad8c3-2e9c-4d59-afea-3a913d6c08fd"
        },
        {
            "id": "51882fcb-f720-4c6f-9a8d-91b66fb05ac2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "088ad8c3-2e9c-4d59-afea-3a913d6c08fd"
        },
        {
            "id": "f625935f-8948-4c90-92e5-abd8a8d7740e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "088ad8c3-2e9c-4d59-afea-3a913d6c08fd"
        },
        {
            "id": "0249cc2b-1451-470c-8d3f-4e28cc79c2ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "088ad8c3-2e9c-4d59-afea-3a913d6c08fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}