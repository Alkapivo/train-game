///@description Initialize

	global.guiWidth = 960;
	global.guiHeight = 540;
	
	var pilot = global.pilotTypes[global.pilotType];
	global.audioContext = pilot[PilotType.SOUNDTRACK];
	if (global.enableAudio) {
		if (!audio_is_playing(global.audioContext)) {
			audio_play_sound(global.audioContext, 100, true)	;
		}
	}
	
	leftPoint = 0;
	rightPoint = 0;

	timer = 0;
	timerCounter = 0;
	horizontalOffset = 0;
	
	mouseTimer = 0;
	mouseTimerMax = room_speed * 0.75;
	mouseIncrement = 1.0;
	mouseDecrement = 1.0;
	mouseFreeze = false;
	mouseChecked = false;
	
	global.renderLayer = layer_get_id("render_layer");
	global.pilotLayer = layer_get_id("pilot_layer");
	
	global.gameControllerContext = id;
	global.pilotContext = instance_create_layer(
		0, 0, global.pilotLayer, Pilot);
		
	messagePipeline = ds_stack_create();
	messageList = ds_list_create();
	
	spriteTheta = 0;
	spriteMargin = 16;
	
	cursorTheta = 0;
	
	view_enabled = true;
	angle = 0;
	camera = camera_create_view(0, 0, global.guiWidth, global.guiHeight, angle);
	view = 0;
	view_set_camera(view, camera);
	
	cameraShake = pi * 8;
	cameraIsShaking = false;
	
	
	red = 120;
    green = 30;
    blue = 240;
    redDirection = 1;
    greenDirection = 1;
    blueDirection = -1;
	hitBlendColorEnable = false;
	hitBlendColor = c_red;