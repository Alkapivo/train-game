///@description Game loop


	leftPoint = [window_view_mouse_get_x(view), window_view_mouse_get_y(view)];
	rightPoint = [global.guiWidth - window_view_mouse_get_x(view), window_view_mouse_get_y(view)];
	
	mouseChecked = mouse_check_button(mb_left);
	
	if ((mouseChecked) &&
		(!mouseFreeze)) {
			
		mouseTimer += mouseIncrement;
		if (mouseTimer > mouseTimerMax) {
			mouseFreeze = true;
		}
	}
	
	if (mouseFreeze) {
		mouseChecked = false;
		mouseTimer -= mouseDecrement;
		if (mouseTimer <= 0) {
			mouseFreeze = false;	
		}
	}
	
	
	///@description Game loop

	timerCounter++;
	if (timerCounter > room_speed) {
		timer++;
		timerCounter = 0;
		
	var repeatCounter = round(random(5));
		repeat(repeatCounter) {
			var class = choose(Shroom, Shroom, Alcohol);
			instance_create_layer(
				random(global.guiWidth),
				random(global.guiHeight),
				global.renderLayer,
				class);
		}
	}
	
	
	var stackSize = ds_stack_size(messagePipeline)
	repeat(stackSize) {
		var message = ds_stack_pop(messagePipeline);
		ds_list_add(messageList, [ 120, message ]);
	}
	
	for (var index = ds_list_size(messageList) - 1; index >= 0; index--) {
		var messageTask = messageList[| index];
		messageTask[@ 0] = messageTask[0] - 1;
		if (messageTask[@ 0] <= 0) {
			ds_list_delete(messageList, index);
		}
	}
	
	
	
	if (red >= 254) || (red <= 1) {
        redDirection = redDirection * -1;
    }
    if (green >= 254) || (green <= 1) {
        greenDirection = greenDirection * -1;
    }
    if (blue >= 254) || (blue <= 1) {
        blueDirection = blueDirection * -1;
    }
	
	red += 1 * redDirection
    green += 1 * greenDirection
    blue += 1 * blueDirection
	