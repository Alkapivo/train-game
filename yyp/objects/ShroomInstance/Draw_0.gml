/// @description Insert description here
// You can write your code in this editor

	spriteXScale -= scaleFactor;
	spriteYScale -= scaleFactor;
	
	spriteTheta += 0.02;
	if (spriteTheta > pi * 2) {
		spriteTheta = 0;	
	}

	spriteAngle = sin(spriteTheta) * spriteModifier;
	
	draw_sprite_ext(
		sprite_index,
		image_index,
		x,
		y,
		spriteXScale,
		spriteYScale,
		spriteAngle,
		image_blend,
		image_alpha);
	
	draw_sprite_ext(
		asset_shroom_target,
		sprite_get_number(asset_shroom_target),
		x,
		y,
		spriteXScale,
		spriteYScale,
		spriteAngle * 10,
		c_white,
		0.8);
	