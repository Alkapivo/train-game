///@description Constructor

	spriteXScale = 1.0;
	spriteYScale = 1.0;
	spriteTheta = 0;
	spriteAngle = 0;
	scaleFactor = choose(0.001, 0.002, 0.005, 0.01);
	spriteModifier = choose(2, 3, 4, 5, 6, 7, 8);
	
	
	destPoint = [
		random(global.guiWidth),
		random(global.guiHeight)
	];
	
	destSpeed = choose(1, 1, 1, 2, 2, 3, 4, 5);
	
	shroomText = "";
	collisionRadius = 64; // MAGIC NUMBER AF
	