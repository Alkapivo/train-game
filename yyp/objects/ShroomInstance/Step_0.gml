///@description Object loop

	if ((spriteXScale < 0) || (spriteYScale < 0)) {
		instance_destroy();
	}

	if (point_distance(x, y, destPoint[0], destPoint[1]) > destSpeed + 0.1) {
		move_towards_point(
			destPoint[0],
			destPoint[1],
			destSpeed);
	} else {
		x = destPoint[0];
		y = destPoint[1];
	}

		
		
		
	///@description Collision message event

	var gameController = global.gameControllerContext;
	

	
	if (gameController.mouseChecked) {
		
		var leftPoint = gameController.leftPoint;
		var rightPoint = gameController.rightPoint;
		
		if ((point_distance(x, y, leftPoint[0], leftPoint[1]) 
			< collisionRadius) ||
			(point_distance(x, y, rightPoint[0], rightPoint[1]) 
			< collisionRadius)) {
		
			ds_stack_push(gameController.messagePipeline, shroomText);
	
			if (!audio_is_playing(asset_sound_shoot_01)) {
				audio_master_gain(0.33);
				audio_play_sound(asset_sound_shoot_01, 200, false);
				audio_master_gain(1.00);
			}
	
			instance_destroy();
		
		}
	}