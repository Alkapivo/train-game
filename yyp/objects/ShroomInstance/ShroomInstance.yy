{
    "id": "f220c86a-f81e-4d39-836d-d15c8adabe63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ShroomInstance",
    "eventList": [
        {
            "id": "952a69cb-dfc0-4751-80e0-c83053d97c0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f220c86a-f81e-4d39-836d-d15c8adabe63"
        },
        {
            "id": "0a18dfd9-02d7-442f-b771-f6a927e98302",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f220c86a-f81e-4d39-836d-d15c8adabe63"
        },
        {
            "id": "80f1849f-8a51-42e4-a702-6393957cf303",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f220c86a-f81e-4d39-836d-d15c8adabe63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d366cf47-64b8-4a36-a0b0-5e7e09780fd2",
    "visible": true
}