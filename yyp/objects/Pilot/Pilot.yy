{
    "id": "f3ed9d5c-f7f5-4551-8a9b-9a29601507ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Pilot",
    "eventList": [
        {
            "id": "58cf7dca-ac35-4bab-9882-87b348e9d544",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3ed9d5c-f7f5-4551-8a9b-9a29601507ac"
        },
        {
            "id": "4edb6867-ed21-4ee0-96a5-002e95660e7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f3ed9d5c-f7f5-4551-8a9b-9a29601507ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d9cc81b-78f3-4be7-8e8e-8f642cc775bb",
    "visible": true
}