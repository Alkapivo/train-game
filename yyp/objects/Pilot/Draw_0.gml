///@Draw Pilot

	spriteTheta += 0.1;
	if (spriteTheta > pi * 2) {
		spriteTheta = 0;	
	}

	var sprite = sprite_index;
	var spriteX = global.guiWidth / 2.0;
	var spriteY = global.guiHeight + cos(spriteTheta) * 6 + spriteMargin;
	var spriteXScale = 1.0 + cos(spriteTheta) * 0.01;
	var spriteYScale = 1.0 + sin(spriteTheta) * 0.01;;
	var spriteAngle = sin(spriteTheta) * 2;
	var spriteColor = c_white;

	draw_sprite_ext(
		sprite,
		0,
		spriteX,
		spriteY,
		spriteXScale,
		spriteYScale,
		spriteAngle,
		spriteColor,
		1.0);


	var game = global.gameControllerContext; // Hack :|
		
	var pilot = global.pilotTypes[global.pilotType];
	var _leftEye = pilot[PilotType.LEFT_POINT];
	var _rightEye = pilot[PilotType.RIGHT_POINT];
	var leftEye = [ 
		spriteX + _leftEye[0], 
		spriteY + _leftEye[1] + cos(spriteTheta) * 6 + spriteMargin 
	];
	var rightEye = [ 
		spriteX + _rightEye[0], 
		spriteY + _rightEye[1] + cos(spriteTheta) * 6 + spriteMargin 
	];
	
	if (game.mouseChecked) {
		var pointer = 0;
		var laserSizes = [ 16, 7, 4, 2 ];
		var laserColors = [ c_red, c_yellow, c_white, c_fuchsia ];
		repeat(array_length_1d(laserSizes) - 1) {
			var shakeX = random(5);
			var shakeY = random(5);
			var laserSize = laserSizes[pointer];
			var laserColor = laserColors[pointer];
			draw_set_color(laserColor);
			draw_line_width(
				leftEye[0] - shakeX,
				leftEye[1] - shakeY,
				game.leftPoint[0] + shakeX,
				game.leftPoint[1] + shakeY,
				laserSize);
		
			draw_line_width(
				rightEye[0] + shakeX,
				rightEye[1] + shakeY,
				game.rightPoint[0] - shakeX,
				game.rightPoint[1] - shakeY,
				laserSize);
			pointer++;
		}
				
		draw_set_color(c_lime);
		draw_circle(
			game.leftPoint[0],
			game.leftPoint[1],
			(game.mouseTimerMax - game.mouseTimer) * 0.5,
			false);
		draw_circle(
			game.rightPoint[0],
			game.rightPoint[1],
			(game.mouseTimerMax - game.mouseTimer) * 0.5,
			false);
	}
	