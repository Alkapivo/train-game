///@description Initialize
	
	var pilot = global.pilotTypes[global.pilotType];
	sprite_index = pilot[PilotType.AVATAR];
	
	spriteTheta = 0;
	spriteMargin = 0;
	
	red = 120;
    green = 30;
    blue = 240;
    redDirection = 1;
    greenDirection = 1;
    blueDirection = -1;
