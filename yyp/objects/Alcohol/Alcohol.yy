{
    "id": "e95020ba-5f60-4a91-baa1-537e609f7d31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Alcohol",
    "eventList": [
        {
            "id": "ebaf4b90-6ebf-4556-a222-992d49f8b6c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e95020ba-5f60-4a91-baa1-537e609f7d31"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f220c86a-f81e-4d39-836d-d15c8adabe63",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2d82fa8-ad5b-47de-a776-316975fc252a",
    "visible": true
}